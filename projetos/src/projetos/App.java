package projetos;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		//exemplos();
	}
	
	public static void exemplos() {
		int numero = 10;
		double numeroDecimal2 = 9.9;
		float numeroDecimal = 34f;
		char umCaractere = 'A';
		boolean eVerdadeiro = false;

//	    Condicionais

		if (numero == 10) {
			System.out.println(numero);
		} else if (numero == 20) {
			System.out.println("Nem aqui");
		} else {
			System.out.println("Nao vai entrar aqui");
		}
		
		
//		Lacos
		for(int i = 0; i < 5; i++) {
			//	System.out.println(i);
		}
		
		int j = 0;
		while( j < 5) {
			System.out.println(j);
			j++;
		}
		
//		Vetores
		
		int[] numeros = {1,2,3};
		int[] novosNumeros = new int[10];
		
		System.out.println(numeros[2]);
		novosNumeros[5] = 30;
		
		String[] palavras = {"celular", "batata", "frango"};
		
		for(String palavra: palavras) {
			System.out.println(palavra);
		}
		
//		Matriz Biimensional
		int[][] chamaaaaOApp = new int[2][2];
		chamaaaaOApp[0][0] = 0;
		chamaaaaOApp[0][1] = 1;
		chamaaaaOApp[1][0] = 2;
		chamaaaaOApp[1][1] = 3;

		for(int x = 0 ; x < chamaaaaOApp.length; x++) {
			for(int z = 0 ; z < chamaaaaOApp.length; z++) {
				System.out.println(chamaaaaOApp[x][z]);
			}
		}
		
		Scanner scan = new Scanner(System.in);
//		int numeroEscolhido = scan.nextInt();
		System.out.println("Digite o nome:");
		String dadosUsuario = scan.next();
		System.out.println(dadosUsuario);
		
		
		try {
			int numeroConvertido = Integer.parseInt(dadosUsuario);
			System.out.println(numeroConvertido + 1);
		}catch(Exception ex) {
			System.out.println("Nao foi possivel converter numero");
		}
		
//		Cria e lanca uma excecao
//		throws new Exception("Exemplo");
		
		System.out.println("Fim");
		
		String umaString = "";
		
		String str1  = scan.nextLine();
		String str2  = scan.nextLine();
		
		System.out.println(str1.equals(str2));
		scan.close();

		double sorteio = Math.random();
		sorteio = Math.ceil(sorteio * 6);
		System.out.println(sorteio);
		
		
//		Exemplo e gravacao de arquivo
		
		Path path = Paths.get("exemplo.txt");
		String umaString1 = "Vou gravar um arquivo";
		
		try {
			Files.write(path, umaString1.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Exemplo e leitura de arquivo
		List<String> teste = null ;
		try {
		teste  = Files.readAllLines(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(teste);	
	}

}
