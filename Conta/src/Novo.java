import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Novo {
	public static void main(String[] args) {
		
		Conta contaDoSilvioSantos = new Conta(4456,5623,1,"PRIVATE");
		
		Conta contaDoRenan = new Conta(4456,56236,2,"PRIVATE");
		
		System.out.println(contaDoSilvioSantos.sacar(10));
		
		contaDoSilvioSantos.depositar(1000);
		
		System.out.println(contaDoSilvioSantos.getSaldo());
		System.out.println(contaDoSilvioSantos.toString());
		
		
		HashSet<String> pokemons = new HashSet<>();
		pokemons.add("Agua");
		pokemons.add("Fogo");
		pokemons.add("Planta");
		pokemons.add("Fantasma");
		
		System.out.println(pokemons.contains("Fogo"));
		System.out.println(pokemons);
		
		
		ArrayList<String> array = new ArrayList<>();
		array.add("C");
		array.add("COBOL");
		array.add("PHP");
		array.add("C");
		
		System.out.println(array.contains("Fogo"));
		System.out.println(array.size());
		
		for(String ling : array) {
			System.out.println(ling);
		}
		
		
		HashMap<String, String> pessoa = new HashMap<>();
		pessoa.put("Nome", "Renan");
		
		pessoa.containsKey("teste");
		System.out.println(pessoa.get("Nome"));

		array.forEach( ling -> {
			System.out.println(ling);
		});


	}
}
