
public class Conta {
	
	private int agencia;
	private int numero;
	private int digitoVerificador;
	private double saldo;
	private String tipo;
	
	public Conta(int agencia, int numero, int digitoVerificador,String tipo) {
		setAgencia(agencia);
		setNumero(numero);
		setDigitoVerificador(digitoVerificador);
		setTipo(tipo);
		setSaldo(0);
	}
	
	public boolean sacar(double valor) {
		if(this.saldo >= valor) {
			this.saldo -= valor;
			return true;
		}
		
		return false;
	}
	
	public void depositar(double valor){
		this.saldo += valor;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(int digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String toString() {
		return getAgencia() + "|" + getNumero() + "|" + getDigitoVerificador();
	}


}
